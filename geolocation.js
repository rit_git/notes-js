
//getPos obtiene las coordenadas GPS (latitud y longitud)
function getPos(position) {
    const { latitude: lat, longitude : lng } = position.coords;
    console.log({lat,lng});
}

// getErr identifica y muestra un mensaje de error (si ocurre)
function getErr(err) {
    const errors = {
        PERMISSION_DENIED:    'Usuario ha denegado la geolocalización',
        POSITION_UNAVAILABLE: 'Localización no disponible',
        TIMEOUT:              'La operación ha tardado demasiado tiempo',
        UNKNOWN_ERROR:        'Error desconocido'
    };
    console.warn(errors[err]);
}

navigator.geolocation.getCurrentPosition(getPos, getErr);