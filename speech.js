let voices, utterance;

function speakVoice() {
    voices = this.getVoices();
    utterance = new SpeechSynthesisUtterance("Prueba de Auido, Hola desde México");
    // utterance.voice = voices[1];
    utterance.voice = voices.find( r => r.lang === "es-ES" )
    speechSynthesis.speak(utterance);
};

speechSynthesis.addEventListener('voiceschanged', speakVoice);

function speakVoiceInstance(){
    const su = new SpeechSynthesisUtterance();
    su.text = "Hola Amiguito desde Google españa";
    const voices = speechSynthesis.getVoices();
    const langVoice = voices.find( r => r.lang === "es-ES" )
    su.voice = langVoice;
    speechSynthesis.speak(su);
}